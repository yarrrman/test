<?php
/* нативный рендерер вьюх */
namespace App\Renderer;
require_once 'Template.php';
function render($name, $variables=[]){
    $path = implode(DIRECTORY_SEPARATOR,[
             getcwd(),
             'views',
             $name.".phtml"
            ]);

   return \App\Template\render($path, $variables);
}
