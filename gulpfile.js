var gulp = require('gulp');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var less = require('gulp-less');


/*
таск для сборки JS
на входе используются (es2015 + система модулей из esNext)
 */
gulp.task('build', function () {
    return browserify({entries: './src/app.js', debug: true})
        .transform('babelify', {
          sourceMapsAbsolute: true,
          presets:['es2015']
        })
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('views/dist'));
});

/*
  сборка Less
*/
gulp.task('less', function(){
  return gulp
    .src('./views/css/style.less')
    .pipe(less())
    .pipe(gulp.dest('./views/css'));
});


gulp.task('watch', function(){
  gulp.watch(['./src/*.js', './views/css/*.less'], ['build', 'less']);
});
