<?php


require_once './vendor/autoload.php';


/*Подключение роутера */
require_once 'Application.php';
/* Подключение класса игры */
require_once 'Game.php';


/* Подключаем Twig */
$loader = new Twig_Loader_Filesystem('./views');
$twig = new Twig_Environment($loader, array(
    'debug'=>true
));

/* инстанс игры*/
$game = new App\Game\Game();
/* инстанс приложения */
$app = $app = new App\Application();


$app->get('/', function () use ($game, $twig) {
  /*
   Инициализация игры
   сброс начальных значений
   */
   $game->initGame();

   /*
   рендер страницы
   передаем параметрами начальное состояние
   и правила игра
   */
   return $twig->render('index.html', ['init'=> json_encode([
                         'state'=>$game->getState(),
                         'rules'=>$game->rules   ])
                      ]);
});

$app->get('/movement', function () use ($game) {
  /*
  роут обработки хода
  */
   return $game->movement();
});

$app->get('/state', function() use($game){
  /*
  Начальное состояние и правила можно запросить отдельно
  */
  return json_encode([
    'state'=>$game->getState(),
    'rules'=>$game->rules
  ]);
});

/*Запуск роутера*/
$app->run();
