(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _parser = require('./parser.js');

var _parser2 = _interopRequireDefault(_parser);

var _game = require('./game.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//DEPRECATED


window.game;
//Модуль игры

$(function () {
  try {
    // Начальное состояние со страницы
    var initState = JSON.parse(initGameString);
    //инициализируем игру
    window.game = new _game.Game(initState);
  } catch (err) {
    //при ошибке парсинга на странице альтернативный вариант запроса начального состояния
    console.log(err);

    $.getJSON('/state', function (data) {
      window.game = new _game.Game(initState);
    });
  }
});

},{"./game.js":2,"./parser.js":3}],2:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Game = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _renderer = require("./renderer.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Game = function () {
  function Game(init) {
    _classCallCheck(this, Game);

    //забираем правила
    debugger;
    this.rules = init["rules"];
    // получем начальный стейт
    this.state = init["state"];
    // координаты пимпы для ячеек
    this.fieldPoints = this.rulesInit();
    // рендерим html
    this.renderHandler(null, false);
    // прописываем события
    this.handlersInit([{ event: 'click', selector: '.game-button', handler: this.movementHandler }]);
  }

  // описание событий


  _createClass(Game, [{
    key: "handlersInit",
    value: function handlersInit(handlers) {
      handlers.forEach(function (handler) {
        $(handler.selector).on(handler.event, handler.handler.bind(this));
      });
    }
  }, {
    key: "renderHandler",
    value: function renderHandler(state) {
      var isMovement = arguments.length <= 1 || arguments[1] === undefined ? true : arguments[1];

      state = state || this.state;
      // отрисовываем игровое поле по переданному или текущему состоянию
      (0, _renderer.render)(state, isMovement);
      return state;
    }
  }, {
    key: "movementHandler",
    value: function movementHandler() {
      debugger;
      var rules = game.rules;
      var points = game.fieldPoints;
      var renderHandler = game.renderHandler;

      if (game.state.movement <= 0) {
        alert("Все ходы закончились! \n Перезапустите игру (перезагрузить страницу)");
        return;
      }

      $('.game-button').prop('disabled', true);
      $('.dice').css({ "visibility": "hidden" });

      Promise.all([new Promise(function (resolve, reject) {
        /* 2.1 Animated hands */
        /*
          руку анимуируем из JS так как события на csы-анимацию нет
        */
        var $div = $('.handsAnim');
        // используем rx для генерации последовательностей по времени
        // начальное ожидание, интервал , первый 11 тиков
        var source = Rx.Observable.timer(0, 67).take(11);
        // запуск шедулера
        source.subscribe(function (x) {
          // событие по таймеру
          // смещаем бакграунд на нужное количество пикселей
          console.log(x);
          var off = -(x + 1) * 77;
          $div.css({
            'background-position-x': off + "px"
          });
        }, function (e) {
          // обработка ошибки
          return console.log(e);
        },
        // функция вызываемая по завершению шедулера
        function () {
          return resolve();
        });
      }), new Promise(function (resolve, reject) {
        /* 2.2 Server request */
        // ззапрос данных о ходе
        $.getJSON('/movement', function (data) {
          console.log(data);
          // помещаем новые данные в стейт
          // не самая удачная архитектура, поэтому приходится, пока писать глобально в стейт
          // необходим рефакторинг
          window.game.state = data;
          //this.state = this.renderHandler(data);
          return resolve();
        });
      })]).then(function (res) {
        // отрисовываем кость
        var offset = -(game.state['steps'] - 1) * 67;
        $('.dice').css({
          "visibility": "visible",
          'background-position-y': offset + "px"
        });
        // вызываем перерисовку рук
        renderHandler(game.state, true);
        return true;
      }, function (err) {
        return console.log(err);
      }).then(function (x) {
        // функция установки пимпы по координатам от переданной позиции
        function setPimpaPosition(position) {
          var _points$position = _slicedToArray(points[position], 2);

          var x = _points$position[0];
          var y = _points$position[1];

          $('#pimpa').css({
            "left": x + "px",
            "top": y + "px"
          });
        };
        //инициализаци шедулера анимацииперемещений пимпы
        var source = Rx.Observable.timer(0, 250).take(game.state.steps);

        // получаем начальное значение пимпы и устанавливаем
        var curPosition = game.state.curPosition;
        setPimpaPosition(curPosition);
        source.subscribe(function (x) {
          // движение пимпы на следующую ячейку
          // расчет следующей позиции
          var pos = curPosition + x + 1 > 19 ? curPosition + x + 1 - 20 : curPosition + x + 1;

          setPimpaPosition(pos);
        }, function (err) {
          return console.log(err);
        }, function () {
          setTimeout(function () {
            // устанавливаем конечное движение пимпы из правила next
            setPimpaPosition(game.state.endPosition);
            // кнопку делаем вновь доступной
            $('.game-button').prop('disabled', false);
          }, 250);
          // количество оставшихся ходов
          $('.value-dice').text(game.state.movement);
          // бонусные очки
          $('.bonus-score h2').text(game.state.bonusScore);
          //суммарные очки
          $('.total-score h2').text(game.state.totalScore);
        });
      }, function (e) {});
    }
  }, {
    key: "rulesInit",
    value: function rulesInit() {
      // координаты пимпы в ячейках

      return [[210, 130], [300, 130], [390, 130], [480, 130], [570, 130], [660, 130], [750, 130], [750, 210], [750, 290], [750, 370], [750, 450], [660, 450], [570, 450], [480, 450], [390, 450], [300, 450], [210, 450], [210, 370], [210, 290], [210, 210]];
    }
  }]);

  return Game;
}();

exports.Game = Game;

},{"./renderer.js":4}],3:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (arrayOfObject) {
  return arrayOfObject.reduce(function (a, b) {
    return Object.assign(a, b);
  }, {});
};

},{}],4:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var render = function render(initState) {
  var isMovement = arguments.length <= 1 || arguments[1] === undefined ? true : arguments[1];


  // если это ход
  if (isMovement) {
    //рендерим количество рук
    var handsDiv = "" + function () {
      var html = "";
      //невидимы руки - что ыб не ползла верстка
      for (var i = 0; i < 7 - initState.movement; i++) {
        html += "<div class=\"hand\" style=\"visibility:hidden\"></div>";
      }
      // видимые руки
      for (var i = 1; i < initState.movement; i++) {
        html += "<div class=\"hand\"></div>";
      }
      return html;
    }() + (
    // если ходы есть то последняя рука анимированная
    initState['movement'] > 0 ? '<div class="hand handsAnim"></div>' : '');
    // перерисовываем руки
    $('.hands').html(handsDiv);
  } else {
    // рендеринг HTML игрового поля игры

    var tmpl = '<div class="field">' + '<div id="pimpa"></div>' + '<div class="hands">' + function () {
      var html = "";
      for (var i = 1; i < initState.movement; i++) {
        html += "<div class='hand'></div>";
      }
      return html;
    }() + '<div class="hand handsAnim"></div>' + '</div>' + '<div class="text-dice"><h2><span class="value-dice">7</span> EMPTY DICE\'S<h2></div>' + '<div class="score">' + '<div class="bonus-score">' + '<h2>0</h2>' + '</div>' + '<div class="total-score"><h2>0</h2></div>' + '</div>' + '<div class="dice"></div>' + ("<button class=\"game-button\" " + (initState.movement > 0 ? "" : "disabled") + " >Start</button>") + '</div>';
    $(".game-field").html(tmpl);
  }
};

exports.render = render;

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJGOlxcRG9jc1xcRGV2ZWxvcFxcZ2FtZVxcdGVzdFRhc2tcXDNcXHNyY1xcYXBwLmpzIiwiRjpcXERvY3NcXERldmVsb3BcXGdhbWVcXHRlc3RUYXNrXFwzXFxzcmNcXGdhbWUuanMiLCJGOlxcRG9jc1xcRGV2ZWxvcFxcZ2FtZVxcdGVzdFRhc2tcXDNcXHNyY1xccGFyc2VyLmpzIiwiRjpcXERvY3NcXERldmVsb3BcXGdhbWVcXHRlc3RUYXNrXFwzXFxzcmNcXHJlbmRlcmVyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7QUNDQTs7OztBQUVBOzs7Ozs7O0FBRUEsT0FBTyxJQUFQOzs7QUFDQSxFQUFFLFlBQVU7QUFDUixNQUFJOztBQUVGLFFBQUksWUFBWSxLQUFLLEtBQUwsQ0FBVyxjQUFYLENBQWhCOztBQUVBLFdBQU8sSUFBUCxHQUFjLGVBQVMsU0FBVCxDQUFkO0FBQ0QsR0FMRCxDQUtFLE9BQU8sR0FBUCxFQUFZOztBQUVaLFlBQVEsR0FBUixDQUFZLEdBQVo7O0FBRUEsTUFBRSxPQUFGLENBQVUsUUFBVixFQUFvQixVQUFTLElBQVQsRUFBYztBQUM5QixhQUFPLElBQVAsR0FBYyxlQUFTLFNBQVQsQ0FBZDtBQUNILEtBRkQ7QUFHRDtBQUNKLENBZEQ7Ozs7Ozs7Ozs7Ozs7O0FDTEE7Ozs7SUFFTSxJO0FBR0YsZ0JBQVksSUFBWixFQUFpQjtBQUFBOzs7QUFFZjtBQUNBLFNBQUssS0FBTCxHQUFhLEtBQUssT0FBTCxDQUFiOztBQUVBLFNBQUssS0FBTCxHQUFhLEtBQUssT0FBTCxDQUFiOztBQUVBLFNBQUssV0FBTCxHQUFtQixLQUFLLFNBQUwsRUFBbkI7O0FBRUEsU0FBSyxhQUFMLENBQW1CLElBQW5CLEVBQXlCLEtBQXpCOztBQUVBLFNBQUssWUFBTCxDQUFrQixDQUFDLEVBQUUsT0FBTyxPQUFULEVBQWtCLFVBQVUsY0FBNUIsRUFBNEMsU0FBUyxLQUFLLGVBQTFELEVBQUQsQ0FBbEI7QUFDRDs7Ozs7OztpQ0FHWSxRLEVBQVU7QUFDdEIsZUFBUyxPQUFULENBQWlCLFVBQVUsT0FBVixFQUFtQjtBQUNsQyxVQUFFLFFBQVEsUUFBVixFQUFvQixFQUFwQixDQUF1QixRQUFRLEtBQS9CLEVBQXNDLFFBQVEsT0FBUixDQUFnQixJQUFoQixDQUFxQixJQUFyQixDQUF0QztBQUNELE9BRkQ7QUFHRDs7O2tDQUVhLEssRUFBd0I7QUFBQSxVQUFqQixVQUFpQix5REFBTixJQUFNOztBQUNwQyxjQUFRLFNBQVMsS0FBSyxLQUF0Qjs7QUFFQSw0QkFBTyxLQUFQLEVBQWMsVUFBZDtBQUNBLGFBQU8sS0FBUDtBQUNEOzs7c0NBQ2dCO0FBQ2Y7QUFDQSxVQUFJLFFBQVEsS0FBSyxLQUFqQjtBQUNBLFVBQUksU0FBUyxLQUFLLFdBQWxCO0FBQ0EsVUFBSSxnQkFBZ0IsS0FBSyxhQUF6Qjs7QUFFRSxVQUFJLEtBQUssS0FBTCxDQUFXLFFBQVgsSUFBdUIsQ0FBM0IsRUFBOEI7QUFDNUIsY0FBTSxzRUFBTjtBQUNBO0FBQ0Q7O0FBRUosUUFBRSxjQUFGLEVBQWtCLElBQWxCLENBQXVCLFVBQXZCLEVBQW1DLElBQW5DO0FBQ0csUUFBRSxPQUFGLEVBQVcsR0FBWCxDQUFlLEVBQUUsY0FBYyxRQUFoQixFQUFmOztBQUVBLGNBQVEsR0FBUixDQUFZLENBQ2IsSUFBSSxPQUFKLENBQVksVUFBVSxPQUFWLEVBQW1CLE1BQW5CLEVBQTJCOzs7OztBQUt0QyxZQUFJLE9BQU8sRUFBRSxZQUFGLENBQVg7OztBQUdBLFlBQUksU0FBUyxHQUFHLFVBQUgsQ0FBYyxLQUFkLENBQW9CLENBQXBCLEVBQXVCLEVBQXZCLEVBQTJCLElBQTNCLENBQWdDLEVBQWhDLENBQWI7O0FBRUEsZUFBTyxTQUFQLENBQ0QsYUFBSzs7O0FBR0gsa0JBQVEsR0FBUixDQUFZLENBQVo7QUFDQSxjQUFJLE1BQU0sRUFBRSxJQUFJLENBQU4sSUFBVyxFQUFyQjtBQUNBLGVBQUssR0FBTCxDQUFTO0FBQ1YscUNBQXlCLE1BQU07QUFEckIsV0FBVDtBQUdELFNBVEEsRUFVRCxhQUFJOztBQUVGLGlCQUFPLFFBQVEsR0FBUixDQUFZLENBQVosQ0FBUDtBQUNELFNBYkE7O0FBZUQ7QUFBQSxpQkFBTyxTQUFQO0FBQUEsU0FmQztBQWlCRCxPQTNCQSxDQURhLEVBOEJkLElBQUksT0FBSixDQUFZLFVBQVUsT0FBVixFQUFtQixNQUFuQixFQUEyQjs7O0FBR3JDLFVBQUUsT0FBRixDQUFVLFdBQVYsRUFBdUIsVUFBVSxJQUFWLEVBQWdCO0FBQ3hDLGtCQUFRLEdBQVIsQ0FBWSxJQUFaOzs7O0FBSUEsaUJBQU8sSUFBUCxDQUFZLEtBQVosR0FBb0IsSUFBcEI7O0FBRUEsaUJBQU8sU0FBUDtBQUNFLFNBUkQ7QUFTRCxPQVpELENBOUJjLENBQVosRUEyQ0EsSUEzQ0EsQ0E0Q0YsZUFBSzs7QUFFSCxZQUFJLFNBQVMsRUFBRSxLQUFLLEtBQUwsQ0FBVyxPQUFYLElBQXNCLENBQXhCLElBQTZCLEVBQTFDO0FBQ0MsVUFBRSxPQUFGLEVBQVcsR0FBWCxDQUFlO0FBQ2hCLHdCQUFjLFNBREU7QUFFaEIsbUNBQXlCLFNBQVM7QUFGbEIsU0FBZjs7QUFLQSxzQkFBYyxLQUFLLEtBQW5CLEVBQTBCLElBQTFCO0FBQ0EsZUFBTyxJQUFQO0FBQ0YsT0F0REMsRUF1REYsZUFBTTtBQUNMLGVBQU8sUUFBUSxHQUFSLENBQVksR0FBWixDQUFQO0FBQ0EsT0F6REMsRUEwREQsSUExREMsQ0EyREQsYUFBRzs7QUFFRCxpQkFBUyxnQkFBVCxDQUEwQixRQUExQixFQUFvQztBQUFBLGdEQUN2QixPQUFPLFFBQVAsQ0FEdUI7O0FBQUEsY0FDL0IsQ0FEK0I7QUFBQSxjQUM1QixDQUQ0Qjs7QUFFckMsWUFBRSxRQUFGLEVBQVksR0FBWixDQUFnQjtBQUNkLG9CQUFRLElBQUksSUFERTtBQUVkLG1CQUFPLElBQUk7QUFGRyxXQUFoQjtBQUlBOztBQUVELFlBQUksU0FBUyxHQUFHLFVBQUgsQ0FBYyxLQUFkLENBQW9CLENBQXBCLEVBQXVCLEdBQXZCLEVBQTRCLElBQTVCLENBQWlDLEtBQUssS0FBTCxDQUFXLEtBQTVDLENBQWI7OztBQUdBLFlBQUksY0FBYyxLQUFLLEtBQUwsQ0FBVyxXQUE3QjtBQUNBLHlCQUFpQixXQUFqQjtBQUNFLGVBQU8sU0FBUCxDQUNFLGFBQUs7OztBQUdILGNBQUksTUFBTSxjQUFjLENBQWQsR0FBa0IsQ0FBbEIsR0FBc0IsRUFBdEIsR0FBMkIsY0FBYyxDQUFkLEdBQWtCLENBQWxCLEdBQXNCLEVBQWpELEdBQXNELGNBQWMsQ0FBZCxHQUFrQixDQUFsRjs7QUFFSCwyQkFBaUIsR0FBakI7QUFDRCxTQVBBLEVBUUQsZUFBTztBQUNMLGlCQUFPLFFBQVEsR0FBUixDQUFZLEdBQVosQ0FBUDtBQUNELFNBVkEsRUFXRCxZQUFNO0FBQ0oscUJBQVcsWUFBVTs7QUFFcEIsNkJBQWlCLEtBQUssS0FBTCxDQUFXLFdBQTVCOztBQUVBLGNBQUUsY0FBRixFQUFrQixJQUFsQixDQUF1QixVQUF2QixFQUFtQyxLQUFuQztBQUNBLFdBTEQsRUFLRyxHQUxIOztBQU9BLFlBQUUsYUFBRixFQUFpQixJQUFqQixDQUFzQixLQUFLLEtBQUwsQ0FBVyxRQUFqQzs7QUFFQSxZQUFFLGlCQUFGLEVBQXFCLElBQXJCLENBQTBCLEtBQUssS0FBTCxDQUFXLFVBQXJDOztBQUVBLFlBQUUsaUJBQUYsRUFBcUIsSUFBckIsQ0FBMEIsS0FBSyxLQUFMLENBQVcsVUFBckM7QUFDRCxTQXhCQTtBQXlCRCxPQW5HQSxFQW9HRCxhQUFHLENBQUUsQ0FwR0o7QUF1R0g7OztnQ0FFWTs7O0FBR1YsYUFBTyxDQUFDLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FBRCxFQUNOLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FETSxFQUVOLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FGTSxFQUdOLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FITSxFQUlOLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FKTSxFQUtOLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FMTSxFQU1OLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FOTSxFQU9OLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FQTSxFQVFOLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FSTSxFQVNOLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FUTSxFQVVOLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FWTSxFQVdOLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FYTSxFQVlOLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FaTSxFQWFOLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FiTSxFQWNOLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FkTSxFQWVOLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FmTSxFQWdCTixDQUFDLEdBQUQsRUFBTSxHQUFOLENBaEJNLEVBaUJOLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FqQk0sRUFrQk4sQ0FBQyxHQUFELEVBQU0sR0FBTixDQWxCTSxFQW1CTixDQUFDLEdBQUQsRUFBTSxHQUFOLENBbkJNLENBQVA7QUFvQkQ7Ozs7OztRQUVHLEksR0FBQSxJOzs7Ozs7Ozs7a0JDakxPO0FBQUEsU0FBaUIsY0FBYyxNQUFkLENBQXNCLFVBQUMsQ0FBRCxFQUFHLENBQUg7QUFBQSxXQUFPLE9BQU8sTUFBUCxDQUFjLENBQWQsRUFBZ0IsQ0FBaEIsQ0FBUDtBQUFBLEdBQXRCLEVBQWlELEVBQWpELENBQWpCO0FBQUEsQzs7Ozs7Ozs7QUNBZixJQUFJLFNBQVMsU0FBVCxNQUFTLENBQVUsU0FBVixFQUF3QztBQUFBLE1BQW5CLFVBQW1CLHlEQUFOLElBQU07Ozs7QUFHbkQsTUFBSSxVQUFKLEVBQ0E7O0FBRUUsUUFBSSxXQUFXLEtBQUssWUFBWTtBQUM5QixVQUFJLE9BQU8sRUFBWDs7QUFFQSxXQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLElBQUksSUFBSSxVQUFVLFFBQWxDLEVBQTRDLEdBQTVDLEVBQWlEO0FBQy9DLGdCQUFRLHdEQUFSO0FBQ0Q7O0FBRUQsV0FBSyxJQUFJLElBQUksQ0FBYixFQUFnQixJQUFJLFVBQVUsUUFBOUIsRUFBd0MsR0FBeEMsRUFBNkM7QUFDM0MsZ0JBQVEsNEJBQVI7QUFDRDtBQUNELGFBQU8sSUFBUDtBQUVELEtBWm1CLEVBQUw7O0FBY2QsY0FBVSxVQUFWLElBQXdCLENBQXhCLEdBQTRCLG9DQUE1QixHQUFtRSxFQWRyRCxDQUFmOztBQWdCQSxNQUFFLFFBQUYsRUFBWSxJQUFaLENBQWlCLFFBQWpCO0FBQ0QsR0FwQkQsTUFvQk87OztBQUdMLFFBQUksT0FBTyx3QkFBd0Isd0JBQXhCLEdBQW1ELHFCQUFuRCxHQUEyRSxZQUFZO0FBQ2hHLFVBQUksT0FBTyxFQUFYO0FBQ0EsV0FBSyxJQUFJLElBQUksQ0FBYixFQUFnQixJQUFJLFVBQVUsUUFBOUIsRUFBd0MsR0FBeEMsRUFBNkM7QUFDM0MsZ0JBQVEsMEJBQVI7QUFDRDtBQUNELGFBQU8sSUFBUDtBQUNELEtBTnFGLEVBQTNFLEdBTUwsb0NBTkssR0FNa0MsUUFObEMsR0FNNkMsc0ZBTjdDLEdBTXNJLHFCQU50SSxHQU04SiwyQkFOOUosR0FNNEwsWUFONUwsR0FNMk0sUUFOM00sR0FNc04sMkNBTnROLEdBTW9RLFFBTnBRLEdBTStRLDBCQU4vUSxJQU02UyxvQ0FBb0MsVUFBVSxRQUFWLEdBQXFCLENBQXJCLEdBQXlCLEVBQXpCLEdBQThCLFVBQWxFLElBQWdGLGtCQU43WCxJQU1tWixRQU45WjtBQU9BLE1BQUUsYUFBRixFQUFpQixJQUFqQixDQUFzQixJQUF0QjtBQUNEO0FBQ0YsQ0FuQ0Q7O1FBcUNRLE0sR0FBQSxNIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIi8vREVQUkVDQVRFRFxyXG5pbXBvcnQgcGFyc2UgZnJvbSAnLi9wYXJzZXIuanMnO1xyXG4vL9Cc0L7QtNGD0LvRjCDQuNCz0YDRi1xyXG5pbXBvcnQge0dhbWV9IGZyb20gJy4vZ2FtZS5qcyc7XHJcblxyXG53aW5kb3cuZ2FtZTtcclxuJChmdW5jdGlvbigpe1xyXG4gICAgdHJ5IHtcclxuICAgICAgLy8g0J3QsNGH0LDQu9GM0L3QvtC1INGB0L7RgdGC0L7Rj9C90LjQtSDRgdC+INGB0YLRgNCw0L3QuNGG0YtcclxuICAgICAgdmFyIGluaXRTdGF0ZSA9IEpTT04ucGFyc2UoaW5pdEdhbWVTdHJpbmcpO1xyXG4gICAgICAvL9C40L3QuNGG0LjQsNC70LjQt9C40YDRg9C10Lwg0LjQs9GA0YNcclxuICAgICAgd2luZG93LmdhbWUgPSBuZXcgR2FtZShpbml0U3RhdGUpO1xyXG4gICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgIC8v0L/RgNC4INC+0YjQuNCx0LrQtSDQv9Cw0YDRgdC40L3Qs9CwINC90LAg0YHRgtGA0LDQvdC40YbQtSDQsNC70YzRgtC10YDQvdCw0YLQuNCy0L3Ri9C5INCy0LDRgNC40LDQvdGCINC30LDQv9GA0L7RgdCwINC90LDRh9Cw0LvRjNC90L7Qs9C+INGB0L7RgdGC0L7Rj9C90LjRj1xyXG4gICAgICBjb25zb2xlLmxvZyhlcnIpO1xyXG5cclxuICAgICAgJC5nZXRKU09OKCcvc3RhdGUnLCBmdW5jdGlvbihkYXRhKXtcclxuICAgICAgICAgIHdpbmRvdy5nYW1lID0gbmV3IEdhbWUoaW5pdFN0YXRlKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbn0pO1xyXG4iLCJcclxuaW1wb3J0IHtyZW5kZXJ9IGZyb20gJy4vcmVuZGVyZXIuanMnXHJcblxyXG5jbGFzcyBHYW1lXHJcbntcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihpbml0KXtcclxuICAgICAgLy/Qt9Cw0LHQuNGA0LDQtdC8INC/0YDQsNCy0LjQu9CwXHJcbiAgICAgIGRlYnVnZ2VyO1xyXG4gICAgICB0aGlzLnJ1bGVzID0gaW5pdFtcInJ1bGVzXCJdO1xyXG4gICAgICAvLyDQv9C+0LvRg9GH0LXQvCDQvdCw0YfQsNC70YzQvdGL0Lkg0YHRgtC10LnRglxyXG4gICAgICB0aGlzLnN0YXRlID0gaW5pdFtcInN0YXRlXCJdO1xyXG4gICAgICAvLyDQutC+0L7RgNC00LjQvdCw0YLRiyDQv9C40LzQv9GLINC00LvRjyDRj9GH0LXQtdC6XHJcbiAgICAgIHRoaXMuZmllbGRQb2ludHMgPSB0aGlzLnJ1bGVzSW5pdCgpO1xyXG4gICAgICAvLyDRgNC10L3QtNC10YDQuNC8IGh0bWxcclxuICAgICAgdGhpcy5yZW5kZXJIYW5kbGVyKG51bGwsIGZhbHNlKTtcclxuICAgICAgLy8g0L/RgNC+0L/QuNGB0YvQstCw0LXQvCDRgdC+0LHRi9GC0LjRj1xyXG4gICAgICB0aGlzLmhhbmRsZXJzSW5pdChbeyBldmVudDogJ2NsaWNrJywgc2VsZWN0b3I6ICcuZ2FtZS1idXR0b24nLCBoYW5kbGVyOiB0aGlzLm1vdmVtZW50SGFuZGxlciB9XSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8g0L7Qv9C40YHQsNC90LjQtSDRgdC+0LHRi9GC0LjQuVxyXG4gICAgaGFuZGxlcnNJbml0KGhhbmRsZXJzKSB7XHJcbiAgICAgaGFuZGxlcnMuZm9yRWFjaChmdW5jdGlvbiAoaGFuZGxlcikge1xyXG4gICAgICAgJChoYW5kbGVyLnNlbGVjdG9yKS5vbihoYW5kbGVyLmV2ZW50LCBoYW5kbGVyLmhhbmRsZXIuYmluZCh0aGlzKSk7XHJcbiAgICAgfSk7XHJcbiAgIH1cclxuXHJcbiAgIHJlbmRlckhhbmRsZXIoc3RhdGUsIGlzTW92ZW1lbnQ9dHJ1ZSkge1xyXG4gICAgIHN0YXRlID0gc3RhdGUgfHwgdGhpcy5zdGF0ZTtcclxuICAgICAvLyDQvtGC0YDQuNGB0L7QstGL0LLQsNC10Lwg0LjQs9GA0L7QstC+0LUg0L/QvtC70LUg0L/QviDQv9C10YDQtdC00LDQvdC90L7QvNGDINC40LvQuCDRgtC10LrRg9GJ0LXQvNGDINGB0L7RgdGC0L7Rj9C90LjRjlxyXG4gICAgIHJlbmRlcihzdGF0ZSwgaXNNb3ZlbWVudCk7XHJcbiAgICAgcmV0dXJuIHN0YXRlO1xyXG4gICB9XHJcbiAgIG1vdmVtZW50SGFuZGxlcigpe1xyXG4gICAgIGRlYnVnZ2VyO1xyXG4gICAgIHZhciBydWxlcyA9IGdhbWUucnVsZXM7XHJcbiAgICAgdmFyIHBvaW50cyA9IGdhbWUuZmllbGRQb2ludHM7XHJcbiAgICAgdmFyIHJlbmRlckhhbmRsZXIgPSBnYW1lLnJlbmRlckhhbmRsZXI7XHJcblxyXG4gICAgICAgaWYgKGdhbWUuc3RhdGUubW92ZW1lbnQgPD0gMCkge1xyXG4gICAgICAgICBhbGVydChcItCS0YHQtSDRhdC+0LTRiyDQt9Cw0LrQvtC90YfQuNC70LjRgdGMISBcXG4g0J/QtdGA0LXQt9Cw0L/Rg9GB0YLQuNGC0LUg0LjQs9GA0YMgKNC/0LXRgNC10LfQsNCz0YDRg9C30LjRgtGMINGB0YLRgNCw0L3QuNGG0YMpXCIpO1xyXG4gICAgICAgICByZXR1cm47XHJcbiAgICAgICB9XHJcblxyXG4gICBcdCQoJy5nYW1lLWJ1dHRvbicpLnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSk7XHJcbiAgICAgICAkKCcuZGljZScpLmNzcyh7IFwidmlzaWJpbGl0eVwiOiBcImhpZGRlblwiIH0pO1xyXG5cclxuICAgICAgIFByb21pc2UuYWxsKFtcclxuICAgXHQgIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcclxuICAgXHRcdCAgLyogMi4xIEFuaW1hdGVkIGhhbmRzICovXHJcbiAgIFx0XHQvKlxyXG4gICBcdFx0ICDRgNGD0LrRgyDQsNC90LjQvNGD0LjRgNGD0LXQvCDQuNC3IEpTINGC0LDQuiDQutCw0Log0YHQvtCx0YvRgtC40Y8g0L3QsCBjc9GLLdCw0L3QuNC80LDRhtC40Y4g0L3QtdGCXHJcbiAgIFx0XHQqL1xyXG4gICBcdFx0ICB2YXIgJGRpdiA9ICQoJy5oYW5kc0FuaW0nKTtcclxuICAgXHRcdCAgLy8g0LjRgdC/0L7Qu9GM0LfRg9C10Lwgcngg0LTQu9GPINCz0LXQvdC10YDQsNGG0LjQuCDQv9C+0YHQu9C10LTQvtCy0LDRgtC10LvRjNC90L7RgdGC0LXQuSDQv9C+INCy0YDQtdC80LXQvdC4XHJcbiAgIFx0XHQgIC8vINC90LDRh9Cw0LvRjNC90L7QtSDQvtC20LjQtNCw0L3QuNC1LCDQuNC90YLQtdGA0LLQsNC7ICwg0L/QtdGA0LLRi9C5IDExINGC0LjQutC+0LJcclxuICAgXHRcdCAgdmFyIHNvdXJjZSA9IFJ4Lk9ic2VydmFibGUudGltZXIoMCwgNjcpLnRha2UoMTEpO1xyXG4gICBcdFx0XHQvLyDQt9Cw0L/Rg9GB0Log0YjQtdC00YPQu9C10YDQsFxyXG4gICBcdFx0ICBzb3VyY2Uuc3Vic2NyaWJlKFxyXG4gICBcdFx0XHR4ID0+IHtcclxuICAgXHRcdFx0ICAvLyDRgdC+0LHRi9GC0LjQtSDQv9C+INGC0LDQudC80LXRgNGDXHJcbiAgIFx0XHRcdCAgLy8g0YHQvNC10YnQsNC10Lwg0LHQsNC60LPRgNCw0YPQvdC0INC90LAg0L3Rg9C20L3QvtC1INC60L7Qu9C40YfQtdGB0YLQstC+INC/0LjQutGB0LXQu9C10LlcclxuICAgXHRcdFx0ICBjb25zb2xlLmxvZyh4KTtcclxuICAgXHRcdFx0ICB2YXIgb2ZmID0gLSh4ICsgMSkgKiA3NztcclxuICAgXHRcdFx0ICAkZGl2LmNzcyh7XHJcbiAgIFx0XHRcdFx0J2JhY2tncm91bmQtcG9zaXRpb24teCc6IG9mZiArIFwicHhcIlxyXG4gICBcdFx0XHQgIH0pO1xyXG4gICBcdFx0XHR9LFxyXG4gICBcdFx0XHRlID0+e1xyXG4gICBcdFx0XHQgIC8vINC+0LHRgNCw0LHQvtGC0LrQsCDQvtGI0LjQsdC60LhcclxuICAgXHRcdFx0ICByZXR1cm4gY29uc29sZS5sb2coZSk7XHJcbiAgIFx0XHRcdH0sXHJcbiAgIFx0XHRcdC8vINGE0YPQvdC60YbQuNGPINCy0YvQt9GL0LLQsNC10LzQsNGPINC/0L4g0LfQsNCy0LXRgNGI0LXQvdC40Y4g0YjQtdC00YPQu9C10YDQsFxyXG4gICBcdFx0XHQoKSA9PiAgcmVzb2x2ZSgpXHJcbiAgIFx0XHQgICk7XHJcbiAgIFx0XHR9KVxyXG4gICBcdCxcclxuICAgXHQgbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICBcdFx0ICAvKiAyLjIgU2VydmVyIHJlcXVlc3QgKi9cclxuICAgXHRcdCAgLy8g0LfQt9Cw0L/RgNC+0YEg0LTQsNC90L3Ri9GFINC+INGF0L7QtNC1XHJcbiAgIFx0XHQgICQuZ2V0SlNPTignL21vdmVtZW50JywgZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgXHRcdFx0Y29uc29sZS5sb2coZGF0YSk7XHJcbiAgIFx0XHRcdC8vINC/0L7QvNC10YnQsNC10Lwg0L3QvtCy0YvQtSDQtNCw0L3QvdGL0LUg0LIg0YHRgtC10LnRglxyXG4gICBcdFx0XHQvLyDQvdC1INGB0LDQvNCw0Y8g0YPQtNCw0YfQvdCw0Y8g0LDRgNGF0LjRgtC10LrRgtGD0YDQsCwg0L/QvtGN0YLQvtC80YMg0L/RgNC40YXQvtC00LjRgtGB0Y8sINC/0L7QutCwINC/0LjRgdCw0YLRjCDQs9C70L7QsdCw0LvRjNC90L4g0LIg0YHRgtC10LnRglxyXG4gICBcdFx0XHQvLyDQvdC10L7QsdGF0L7QtNC40Lwg0YDQtdGE0LDQutGC0L7RgNC40L3Qs1xyXG4gICBcdFx0XHR3aW5kb3cuZ2FtZS5zdGF0ZSA9IGRhdGE7XHJcbiAgIFx0XHRcdC8vdGhpcy5zdGF0ZSA9IHRoaXMucmVuZGVySGFuZGxlcihkYXRhKTtcclxuICAgXHRcdFx0cmV0dXJuIHJlc29sdmUoKTtcclxuICAgXHRcdCAgfSk7XHJcbiAgIFx0XHR9KVxyXG4gICBcdF0pLnRoZW4oXHJcbiAgIFx0XHRyZXM9PntcclxuICAgICAgICAvLyDQvtGC0YDQuNGB0L7QstGL0LLQsNC10Lwg0LrQvtGB0YLRjFxyXG4gICBcdFx0XHQgdmFyIG9mZnNldCA9IC0oZ2FtZS5zdGF0ZVsnc3RlcHMnXSAtIDEpICogNjc7XHJcbiAgIFx0XHRcdCAgJCgnLmRpY2UnKS5jc3Moe1xyXG4gICBcdFx0XHRcdFwidmlzaWJpbGl0eVwiOiBcInZpc2libGVcIixcclxuICAgXHRcdFx0XHQnYmFja2dyb3VuZC1wb3NpdGlvbi15Jzogb2Zmc2V0ICsgXCJweFwiXHJcbiAgIFx0XHRcdCAgfSk7XHJcbiAgIFx0XHRcdCAgLy8g0LLRi9C30YvQstCw0LXQvCDQv9C10YDQtdGA0LjRgdC+0LLQutGDINGA0YPQulxyXG4gICBcdFx0XHQgIHJlbmRlckhhbmRsZXIoZ2FtZS5zdGF0ZSwgdHJ1ZSk7XHJcbiAgIFx0XHRcdCAgcmV0dXJuIHRydWU7XHJcbiAgIFx0XHR9LFxyXG4gICBcdFx0ZXJyID0+e1xyXG4gICBcdFx0XHRyZXR1cm4gY29uc29sZS5sb2coZXJyKTtcclxuICAgXHRcdH1cclxuICAgXHQpLnRoZW4oXHJcbiAgICAgIHg9PntcclxuICAgICAgICAvLyDRhNGD0L3QutGG0LjRjyDRg9GB0YLQsNC90L7QstC60Lgg0L/QuNC80L/RiyDQv9C+INC60L7QvtGA0LTQuNC90LDRgtCw0Lwg0L7RgiDQv9C10YDQtdC00LDQvdC90L7QuSDQv9C+0LfQuNGG0LjQuFxyXG4gICAgICAgIGZ1bmN0aW9uIHNldFBpbXBhUG9zaXRpb24ocG9zaXRpb24pIHtcclxuICAgXHRcdFx0ICBsZXQgW3gsIHldID0gcG9pbnRzW3Bvc2l0aW9uXTtcclxuICAgXHRcdFx0XHQkKCcjcGltcGEnKS5jc3Moe1xyXG4gICBcdFx0XHRcdCAgXCJsZWZ0XCI6IHggKyBcInB4XCIsXHJcbiAgIFx0XHRcdFx0ICBcInRvcFwiOiB5ICsgXCJweFwiXHJcbiAgIFx0XHRcdFx0fSk7XHJcbiAgIFx0XHRcdH07XHJcbiAgICAgICAgLy/QuNC90LjRhtC40LDQu9C40LfQsNGG0Lgg0YjQtdC00YPQu9C10YDQsCDQsNC90LjQvNCw0YbQuNC40L/QtdGA0LXQvNC10YnQtdC90LjQuSDQv9C40LzQv9GLXHJcbiAgIFx0XHRcdHZhciBzb3VyY2UgPSBSeC5PYnNlcnZhYmxlLnRpbWVyKDAsIDI1MCkudGFrZShnYW1lLnN0YXRlLnN0ZXBzKTtcclxuXHJcbiAgICAgICAgIC8vINC/0L7Qu9GD0YfQsNC10Lwg0L3QsNGH0LDQu9GM0L3QvtC1INC30L3QsNGH0LXQvdC40LUg0L/QuNC80L/RiyDQuCDRg9GB0YLQsNC90LDQstC70LjQstCw0LXQvFxyXG4gICBcdFx0XHR2YXIgY3VyUG9zaXRpb24gPSBnYW1lLnN0YXRlLmN1clBvc2l0aW9uO1xyXG4gICBcdFx0XHRzZXRQaW1wYVBvc2l0aW9uKGN1clBvc2l0aW9uKTtcclxuICAgICAgICBzb3VyY2Uuc3Vic2NyaWJlKFxyXG4gICBcdFx0XHQgICAgeCA9PiB7XHJcbiAgIFx0XHRcdFx0ICAvLyDQtNCy0LjQttC10L3QuNC1INC/0LjQvNC/0Ysg0L3QsCDRgdC70LXQtNGD0Y7RidGD0Y4g0Y/Rh9C10LnQutGDXHJcbiAgICAgICAgICAgIC8vINGA0LDRgdGH0LXRgiDRgdC70LXQtNGD0Y7RidC10Lkg0L/QvtC30LjRhtC40LhcclxuICAgXHRcdFx0XHQgICAgIHZhciBwb3MgPSBjdXJQb3NpdGlvbiArIHggKyAxID4gMTkgPyBjdXJQb3NpdGlvbiArIHggKyAxIC0gMjAgOiBjdXJQb3NpdGlvbiArIHggKyAxO1xyXG5cclxuICAgXHRcdFx0XHQgIHNldFBpbXBhUG9zaXRpb24ocG9zKTtcclxuICAgXHRcdFx0XHR9LFxyXG4gICBcdFx0XHRcdGVyciA9PiB7XHJcbiAgIFx0XHRcdFx0ICByZXR1cm4gY29uc29sZS5sb2coZXJyKTtcclxuICAgXHRcdFx0XHR9LFxyXG4gICBcdFx0XHRcdCgpID0+IHtcclxuICAgXHRcdFx0XHQgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuICAgICBcdFx0XHRcdFx0Ly8g0YPRgdGC0LDQvdCw0LLQu9C40LLQsNC10Lwg0LrQvtC90LXRh9C90L7QtSDQtNCy0LjQttC10L3QuNC1INC/0LjQvNC/0Ysg0LjQtyDQv9GA0LDQstC40LvQsCBuZXh0XHJcbiAgICAgXHRcdFx0XHRcdHNldFBpbXBhUG9zaXRpb24oZ2FtZS5zdGF0ZS5lbmRQb3NpdGlvbik7XHJcbiAgICAgXHRcdFx0XHRcdC8vINC60L3QvtC/0LrRgyDQtNC10LvQsNC10Lwg0LLQvdC+0LLRjCDQtNC+0YHRgtGD0L/QvdC+0LlcclxuICAgICBcdFx0XHRcdFx0JCgnLmdhbWUtYnV0dG9uJykucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XHJcbiAgIFx0XHRcdFx0ICB9LCAyNTApO1xyXG4gICBcdFx0XHRcdCAgLy8g0LrQvtC70LjRh9C10YHRgtCy0L4g0L7RgdGC0LDQstGI0LjRhdGB0Y8g0YXQvtC00L7QslxyXG4gICBcdFx0XHRcdCAgJCgnLnZhbHVlLWRpY2UnKS50ZXh0KGdhbWUuc3RhdGUubW92ZW1lbnQpO1xyXG4gICBcdFx0XHRcdCAgLy8g0LHQvtC90YPRgdC90YvQtSDQvtGH0LrQuFxyXG4gICBcdFx0XHRcdCAgJCgnLmJvbnVzLXNjb3JlIGgyJykudGV4dChnYW1lLnN0YXRlLmJvbnVzU2NvcmUpO1xyXG4gICBcdFx0XHRcdCAgLy/RgdGD0LzQvNCw0YDQvdGL0LUg0L7Rh9C60LhcclxuICAgXHRcdFx0XHQgICQoJy50b3RhbC1zY29yZSBoMicpLnRleHQoZ2FtZS5zdGF0ZS50b3RhbFNjb3JlKTtcclxuICAgXHRcdFx0XHR9KTtcclxuICAgICAgfSxcclxuICAgICAgZT0+e31cclxuICAgICk7XHJcblxyXG4gICB9XHJcblxyXG4gICAgcnVsZXNJbml0KCkge1xyXG4gICAgICAvLyDQutC+0L7RgNC00LjQvdCw0YLRiyDQv9C40LzQv9GLINCyINGP0YfQtdC50LrQsNGFXHJcblxyXG4gICAgICByZXR1cm4gW1syMTAsIDEzMF0sXHJcbiAgICAgICBbMzAwLCAxMzBdLFxyXG4gICAgICAgWzM5MCwgMTMwXSxcclxuICAgICAgIFs0ODAsIDEzMF0sXHJcbiAgICAgICBbNTcwLCAxMzBdLFxyXG4gICAgICAgWzY2MCwgMTMwXSxcclxuICAgICAgIFs3NTAsIDEzMF0sXHJcbiAgICAgICBbNzUwLCAyMTBdLFxyXG4gICAgICAgWzc1MCwgMjkwXSxcclxuICAgICAgIFs3NTAsIDM3MF0sXHJcbiAgICAgICBbNzUwLCA0NTBdLFxyXG4gICAgICAgWzY2MCwgNDUwXSxcclxuICAgICAgIFs1NzAsIDQ1MF0sXHJcbiAgICAgICBbNDgwLCA0NTBdLFxyXG4gICAgICAgWzM5MCwgNDUwXSxcclxuICAgICAgIFszMDAsIDQ1MF0sXHJcbiAgICAgICBbMjEwLCA0NTBdLFxyXG4gICAgICAgWzIxMCwgMzcwXSxcclxuICAgICAgIFsyMTAsIDI5MF0sXHJcbiAgICAgICBbMjEwLCAyMTBdXTtcclxuICAgIH1cclxuICB9XHJcbmV4cG9ydCB7R2FtZX1cclxuIiwiZXhwb3J0IGRlZmF1bHQgYXJyYXlPZk9iamVjdCA9PiBhcnJheU9mT2JqZWN0LnJlZHVjZSggKGEsYik9Pk9iamVjdC5hc3NpZ24oYSxiKSAse30pO1xyXG4iLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24gKGluaXRTdGF0ZSwgaXNNb3ZlbWVudCA9IHRydWUpIHtcclxuXHJcbiAgLy8g0LXRgdC70Lgg0Y3RgtC+INGF0L7QtFxyXG4gIGlmIChpc01vdmVtZW50KVxyXG4gIHtcclxuICAgIC8v0YDQtdC90LTQtdGA0LjQvCDQutC+0LvQuNGH0LXRgdGC0LLQviDRgNGD0LpcclxuICAgIHZhciBoYW5kc0RpdiA9IFwiXCIgKyBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHZhciBodG1sID0gXCJcIjtcclxuICAgICAgLy/QvdC10LLQuNC00LjQvNGLINGA0YPQutC4IC0g0YfRgtC+INGL0LEg0L3QtSDQv9C+0LvQt9C70LAg0LLQtdGA0YHRgtC60LBcclxuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCA3IC0gaW5pdFN0YXRlLm1vdmVtZW50OyBpKyspIHtcclxuICAgICAgICBodG1sICs9IFwiPGRpdiBjbGFzcz1cXFwiaGFuZFxcXCIgc3R5bGU9XFxcInZpc2liaWxpdHk6aGlkZGVuXFxcIj48L2Rpdj5cIjtcclxuICAgICAgfVxyXG4gICAgICAvLyDQstC40LTQuNC80YvQtSDRgNGD0LrQuFxyXG4gICAgICBmb3IgKHZhciBpID0gMTsgaSA8IGluaXRTdGF0ZS5tb3ZlbWVudDsgaSsrKSB7XHJcbiAgICAgICAgaHRtbCArPSBcIjxkaXYgY2xhc3M9XFxcImhhbmRcXFwiPjwvZGl2PlwiO1xyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiBodG1sO1xyXG5cclxuICAgIH0oKSArXHJcbiAgICAvLyDQtdGB0LvQuCDRhdC+0LTRiyDQtdGB0YLRjCDRgtC+INC/0L7RgdC70LXQtNC90Y/RjyDRgNGD0LrQsCDQsNC90LjQvNC40YDQvtCy0LDQvdC90LDRj1xyXG4gICAgKGluaXRTdGF0ZVsnbW92ZW1lbnQnXSA+IDAgPyAnPGRpdiBjbGFzcz1cImhhbmQgaGFuZHNBbmltXCI+PC9kaXY+JyA6ICcnKTtcclxuICAgIC8vINC/0LXRgNC10YDQuNGB0L7QstGL0LLQsNC10Lwg0YDRg9C60LhcclxuICAgICQoJy5oYW5kcycpLmh0bWwoaGFuZHNEaXYpO1xyXG4gIH0gZWxzZSB7XHJcbiAgICAvLyDRgNC10L3QtNC10YDQuNC90LMgSFRNTCDQuNCz0YDQvtCy0L7Qs9C+INC/0L7Qu9GPINC40LPRgNGLXHJcblxyXG4gICAgdmFyIHRtcGwgPSAnPGRpdiBjbGFzcz1cImZpZWxkXCI+JyArICc8ZGl2IGlkPVwicGltcGFcIj48L2Rpdj4nICsgJzxkaXYgY2xhc3M9XCJoYW5kc1wiPicgKyBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHZhciBodG1sID0gXCJcIjtcclxuICAgICAgZm9yICh2YXIgaSA9IDE7IGkgPCBpbml0U3RhdGUubW92ZW1lbnQ7IGkrKykge1xyXG4gICAgICAgIGh0bWwgKz0gXCI8ZGl2IGNsYXNzPSdoYW5kJz48L2Rpdj5cIjtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gaHRtbDtcclxuICAgIH0oKSArICc8ZGl2IGNsYXNzPVwiaGFuZCBoYW5kc0FuaW1cIj48L2Rpdj4nICsgJzwvZGl2PicgKyAnPGRpdiBjbGFzcz1cInRleHQtZGljZVwiPjxoMj48c3BhbiBjbGFzcz1cInZhbHVlLWRpY2VcIj43PC9zcGFuPiBFTVBUWSBESUNFXFwnUzxoMj48L2Rpdj4nICsgJzxkaXYgY2xhc3M9XCJzY29yZVwiPicgKyAnPGRpdiBjbGFzcz1cImJvbnVzLXNjb3JlXCI+JyArICc8aDI+MDwvaDI+JyArICc8L2Rpdj4nICsgJzxkaXYgY2xhc3M9XCJ0b3RhbC1zY29yZVwiPjxoMj4wPC9oMj48L2Rpdj4nICsgJzwvZGl2PicgKyAnPGRpdiBjbGFzcz1cImRpY2VcIj48L2Rpdj4nICsgKFwiPGJ1dHRvbiBjbGFzcz1cXFwiZ2FtZS1idXR0b25cXFwiIFwiICsgKGluaXRTdGF0ZS5tb3ZlbWVudCA+IDAgPyBcIlwiIDogXCJkaXNhYmxlZFwiKSArIFwiID5TdGFydDwvYnV0dG9uPlwiKSArICc8L2Rpdj4nO1xyXG4gICAgJChcIi5nYW1lLWZpZWxkXCIpLmh0bWwodG1wbCk7XHJcbiAgfVxyXG59O1xyXG5cclxuZXhwb3J0IHtyZW5kZXJ9O1xyXG4iXX0=
