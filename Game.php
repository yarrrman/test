<?php
namespace App\Game;
require_once "./Session.php";

/*
класс игры
*/
class Game
{
    /*Добавляем функции работы с сессиями*/
    use \App\Session\sessionManage;
    private $maxPos;
    /*
      описание правил игры
      1. номер в массиве - номер поля
      2. score - количество очков  ячейки
      3. действие с очками - поле action
        3.1 add  - бонус будет равен score сумма = сумма + score
        3.2 изначально вид некоторых ячеек ввел в заблуждение
            по картинке логичным действием было умножения числа выпавшего
            на кубике и значения указанного в ячейке
            можно изменить action - на multiply (например) и SWICH добавить нужное поведение
      4. next - поле ячейки после окончания хода - обработка переходов по стрелкам

      правила отправляются на клиент. что бы не дублировать описание
      возможно имело смысл доавить в них координаты для пимпы
     */
    public $rules = [
      0  => ['score'=> 5, 'action'=>'add' ,'next'=>0 ],
      1  => ['score'=> 15,'action'=>'add' ,'next'=>1 ],
      2  => ['score'=> 15,'action'=>'add' ,'next'=>2],
      3  => ['score'=> 30,'action'=>'add' ,'next'=>3],
      4  => ['score'=> 30,'action'=>'add' ,'next'=>4],
      5  => ['score'=> 10,'action'=>'add' ,'next'=>5],
      6  => ['score'=> 0,'action'=>'add' ,'next'=>6],
      7  => ['score'=> 0,'action'=>'add' ,'next'=>19],
      8  => ['score'=> 50,'action'=>'add' ,'next'=>8],
      9  => ['score'=> 50,'action'=>'add' ,'next'=>9],
      10  => ['score'=> 0,'action'=>'add' ,'next'=>10],
      11  => ['score'=> 20,'action'=>'add' ,'next'=>11],
      12 => ['score'=> 0,'action'=>'add' ,'next'=>4],
      13 => ['score'=> 80,'action'=>'add' ,'next'=>13],
      14 => ['score'=> 80,'action'=>'add' ,'next'=>14],
      15  => ['score'=> 30,'action'=>'add' ,'next'=>15],
      16  => ['score'=> 0,'action'=>'add' ,'next'=>6],
      17 => ['score'=> 120,'action'=>'add' ,'next'=>17],
      18 => ['score'=> 120,'action'=>'add' ,'next'=>18],
      19 => ['score'=> 200,'action'=>'add' ,'next'=>19],
    ];

    public function __construct(){
        /* стартуем сессию */
        $this->initSession();
        $this->maxPos = sizeof($this->rules) - 1;
    }
    public function initGame(){
        /* сбрасываем состояние игры - при загрузке страницы */
        $this->initState();
    }

    private function initState()
    {
      $initState = array(
        'movement'=>7, // Доступные  ходы
        'steps'=>0, // очки на кубике
        'endPosition'=>0, // конечная позиция после броска
        'curPosition'=>0, // текущая позиция пимпы
        'endPositionPreNext'=>0, // не используется
        'bonusScore'=>0, // бонусные очки за ход
        'totalScore'=>0 // сумма очков за игру
      );

      /* Сохранение состояния в сессию */
      $this->putState($initState);
    }

    /* Получение состояния из сессии */
    public function getState()
    {
        return $this->getStateOfSession();
    }

    /* Получение состояния и возврат в формате JSON */
    public function getStateJson()
    {
        return json_encode($this->getState());
    }
    /* Запись состояния в сессию */
    public function putState($state)
    {
      $this->putStateInSession($state);
    }
    /* Обработка хода */
    public function movement()
    {
      /* Получение текущего состояния перед ходом
      // Состояние можно было сделать отдельным классом и вынести работу с ним
      // из класса игры
      Публичные своства такого класса могут быть
      state->isMovment() - досупность ходов
      state->getEmpty() - возврат json с нулевыми movement и steps
      state->movement() - логика хода
      etc
      */

      $state = $this->getState();

      /*
        Если ходов не осталось отправляем клиенту количество доступных ходов
        значение кубика 0
        записываем это состояние в сессию
      */
      if($state['movement'] <= 0){
        $state['movement'] = 0;
        $state['steps'] = 0;
        $this->putState($state);
        return $this->getStateJson();
      }


      $steps = mt_rand(1,6);   // Бросок кубика
      $bonus = 0;               // обнуляем бонусные очки
      $totalScore = $state['totalScore']; // получем сумму очков игры
      $position = $state['endPosition']; // конечная позиция пимпы прошлого хода
      $curPosition = $state['endPosition']; // текущая поиция пимпы берется из конечной позиции прошлого хода

      /*
        проверка на выход из ограничений массива
        поле так же можно вынести в отдельный класс
        и убрать все проверки из общей логики
      */
      $position = ($steps + $position > 19 ? $steps + $position - 20 : $steps + $position);

      //позиция после прохода кубика
      $endPositionPreNext = $position;

      // если позиция не  равна позиции в поле next - значит ячейка перехода
      if($position != $this->rules[$position]['next']){
        $position = $this->rules[$position]['next'];
      }

      //обработка приращения очков
      switch ($this->rules[$position]['action']) {
        // присваеваем бонусу
        case 'add':
          $bonus += $this->rules[$position]['score'];
          break;
          // умножаем на очки на кубике и присваеваем бонусу
        case 'multi':
          $bonus += $this->rules[$position]['score'] * $steps;
          break;
      }
      /* Записываем новое состояние */
      $state['totalScore'] += $bonus; // добавляем бонус в сумму
      $state['movement'] -= 1; // уменьшаем количество ходов на 1
      $state['bonusScore'] = $bonus; // бонус
      $state['steps'] = $steps; // очки на кубике
      $state['endPosition'] = $position; // конечная позиция после броска
      $state['endPositionPreNext'] = $endPositionPreNext;
      $state['curPosition'] = $curPosition;

      // сохраняем в сессию новое состояние
      $this->putState($state);
      // забираем из сессию состояние в JSON
      return $this->getStateJson();
    }
}
