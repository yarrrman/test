<?php
/* Класс роутера*/
namespace App;
//DEPRICATED
require_once 'Renderer.php';

class Application
{
    // обработчики роутов
    private $handlers = [];

    // запись поведения для запросов GET
    public function get($route, $handler)
    {
        $this->append('GET', $route, $handler);
    }
    // запись поведения для запросов POST
    public function post($route, $handler)
    {
        $this->append('POST', $route, $handler);
    }

    public function run()
    {

        $uri = $_SERVER['REQUEST_URI'];
        $method = $_SERVER['REQUEST_METHOD'];
        // поиск маршрута в списке маршрутов
        foreach ($this->handlers as $item) {
            list($route, $handlerMethod, $handler) = $item;
            // экранируем спец символы
            $preparedRoute = preg_quote($route, '/');
            // Если метод запроса и путь существуют в списки роутов
            //выводим результат выполнения функции переданной в описании маршрута
            if ($method == $handlerMethod && preg_match("/^$preparedRoute$/i", $uri)) {
                echo $handler();
            }
        }
    }

    private function append($method, $route, $handler)
    {
        $this->handlers[] = [$route, $method, $handler];
    }
}
