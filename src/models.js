let hands =  [
  {hand:{visibility:true, animated:false}},
  {hand:{visibility:true, animated:false}},
  {hand:{visibility:true, animated:false}},
  {hand:{visibility:true, animated:false}},
  {hand:{visibility:true, animated:false}},
  {hand:{visibility:true, animated:false}},
  {hand:{visibility:true, animated:true}}
];

let rules = [
   [210,130],
   [300,130],
   [390,130],
   [480,130],
   [570,130],
   [660,130],
   [750,130],
   [750,210],
   [750,290],
   [750,370],
   [750,450],
   [660,450],
   [570,450],
   [480,450],
   [390,450],
   [300,450],
   [210,450],
   [210,370],
   [210,290],
   [210,210]
 ];

export {hands, rules}
