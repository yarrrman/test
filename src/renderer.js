var render = function (initState, isMovement = true) {

  // если это ход
  if (isMovement)
  {
    //рендерим количество рук
    var handsDiv = "" + function () {
      var html = "";
      //невидимы руки - что ыб не ползла верстка
      for (var i = 0; i < 7 - initState.movement; i++) {
        html += "<div class=\"hand\" style=\"visibility:hidden\"></div>";
      }
      // видимые руки
      for (var i = 1; i < initState.movement; i++) {
        html += "<div class=\"hand\"></div>";
      }
      return html;

    }() +
    // если ходы есть то последняя рука анимированная
    (initState['movement'] > 0 ? '<div class="hand handsAnim"></div>' : '');
    // перерисовываем руки
    $('.hands').html(handsDiv);
  } else {
    // рендеринг HTML игрового поля игры

    var tmpl = '<div class="field">' + '<div id="pimpa"></div>' + '<div class="hands">' + function () {
      var html = "";
      for (var i = 1; i < initState.movement; i++) {
        html += "<div class='hand'></div>";
      }
      return html;
    }() + '<div class="hand handsAnim"></div>' + '</div>' + '<div class="text-dice"><h2><span class="value-dice">7</span> EMPTY DICE\'S<h2></div>' + '<div class="score">' + '<div class="bonus-score">' + '<h2>0</h2>' + '</div>' + '<div class="total-score"><h2>0</h2></div>' + '</div>' + '<div class="dice"></div>' + ("<button class=\"game-button\" " + (initState.movement > 0 ? "" : "disabled") + " >Start</button>") + '</div>';
    $(".game-field").html(tmpl);
  }
};

export {render};
