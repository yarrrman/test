
import {render} from './renderer.js'

class Game
{

    constructor(init){
      //забираем правила
      debugger;
      this.rules = init["rules"];
      // получем начальный стейт
      this.state = init["state"];
      // координаты пимпы для ячеек
      this.fieldPoints = this.rulesInit();
      // рендерим html
      this.renderHandler(null, false);
      // прописываем события
      this.handlersInit([{ event: 'click', selector: '.game-button', handler: this.movementHandler }]);
    }

    // описание событий
    handlersInit(handlers) {
     handlers.forEach(function (handler) {
       $(handler.selector).on(handler.event, handler.handler.bind(this));
     });
   }

   renderHandler(state, isMovement=true) {
     state = state || this.state;
     // отрисовываем игровое поле по переданному или текущему состоянию
     render(state, isMovement);
     return state;
   }
   movementHandler(){
     debugger;
     var rules = game.rules;
     var points = game.fieldPoints;
     var renderHandler = game.renderHandler;

       if (game.state.movement <= 0) {
         alert("Все ходы закончились! \n Перезапустите игру (перезагрузить страницу)");
         return;
       }

   	$('.game-button').prop('disabled', true);
       $('.dice').css({ "visibility": "hidden" });

       Promise.all([
   	  new Promise(function (resolve, reject) {
   		  /* 2.1 Animated hands */
   		/*
   		  руку анимуируем из JS так как события на csы-анимацию нет
   		*/
   		  var $div = $('.handsAnim');
   		  // используем rx для генерации последовательностей по времени
   		  // начальное ожидание, интервал , первый 11 тиков
   		  var source = Rx.Observable.timer(0, 67).take(11);
   			// запуск шедулера
   		  source.subscribe(
   			x => {
   			  // событие по таймеру
   			  // смещаем бакграунд на нужное количество пикселей
   			  console.log(x);
   			  var off = -(x + 1) * 77;
   			  $div.css({
   				'background-position-x': off + "px"
   			  });
   			},
   			e =>{
   			  // обработка ошибки
   			  return console.log(e);
   			},
   			// функция вызываемая по завершению шедулера
   			() =>  resolve()
   		  );
   		})
   	,
   	 new Promise(function (resolve, reject) {
   		  /* 2.2 Server request */
   		  // ззапрос данных о ходе
   		  $.getJSON('/movement', function (data) {
   			console.log(data);
   			// помещаем новые данные в стейт
   			// не самая удачная архитектура, поэтому приходится, пока писать глобально в стейт
   			// необходим рефакторинг
   			window.game.state = data;
   			//this.state = this.renderHandler(data);
   			return resolve();
   		  });
   		})
   	]).then(
   		res=>{
        // отрисовываем кость
   			 var offset = -(game.state['steps'] - 1) * 67;
   			  $('.dice').css({
   				"visibility": "visible",
   				'background-position-y': offset + "px"
   			  });
   			  // вызываем перерисовку рук
   			  renderHandler(game.state, true);
   			  return true;
   		},
   		err =>{
   			return console.log(err);
   		}
   	).then(
      x=>{
        // функция установки пимпы по координатам от переданной позиции
        function setPimpaPosition(position) {
   			  let [x, y] = points[position];
   				$('#pimpa').css({
   				  "left": x + "px",
   				  "top": y + "px"
   				});
   			};
        //инициализаци шедулера анимацииперемещений пимпы
   			var source = Rx.Observable.timer(0, 250).take(game.state.steps);

         // получаем начальное значение пимпы и устанавливаем
   			var curPosition = game.state.curPosition;
   			setPimpaPosition(curPosition);
        source.subscribe(
   			    x => {
   				  // движение пимпы на следующую ячейку
            // расчет следующей позиции
   				     var pos = curPosition + x + 1 > 19 ? curPosition + x + 1 - 20 : curPosition + x + 1;

   				  setPimpaPosition(pos);
   				},
   				err => {
   				  return console.log(err);
   				},
   				() => {
   				  setTimeout(function(){
     					// устанавливаем конечное движение пимпы из правила next
     					setPimpaPosition(game.state.endPosition);
     					// кнопку делаем вновь доступной
     					$('.game-button').prop('disabled', false);
   				  }, 250);
   				  // количество оставшихся ходов
   				  $('.value-dice').text(game.state.movement);
   				  // бонусные очки
   				  $('.bonus-score h2').text(game.state.bonusScore);
   				  //суммарные очки
   				  $('.total-score h2').text(game.state.totalScore);
   				});
      },
      e=>{}
    );

   }

    rulesInit() {
      // координаты пимпы в ячейках

      return [[210, 130],
       [300, 130],
       [390, 130],
       [480, 130],
       [570, 130],
       [660, 130],
       [750, 130],
       [750, 210],
       [750, 290],
       [750, 370],
       [750, 450],
       [660, 450],
       [570, 450],
       [480, 450],
       [390, 450],
       [300, 450],
       [210, 450],
       [210, 370],
       [210, 290],
       [210, 210]];
    }
  }
export {Game}
