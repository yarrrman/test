//DEPRECATED
import parse from './parser.js';
//Модуль игры
import {Game} from './game.js';

window.game;
$(function(){
    try {
      // Начальное состояние со страницы
      var initState = JSON.parse(initGameString);
      //инициализируем игру
      window.game = new Game(initState);
    } catch (err) {
      //при ошибке парсинга на странице альтернативный вариант запроса начального состояния
      console.log(err);

      $.getJSON('/state', function(data){
          window.game = new Game(initState);
      });
    }
});
