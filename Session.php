<?php
/*
  отделение логики работы с сессиями от кода игры
  инициализации сессии
  сохранение состояния в сессию
  извлечение состояния из сессии
*/
namespace App\Session;
trait sessionManage
{

    function initSession(){
      session_start();
    }

    function putStateInSession($state){
      $_SESSION['state'] = $state;
    }

    function getStateOfSession(){
      return $_SESSION['state'];
    }
}
